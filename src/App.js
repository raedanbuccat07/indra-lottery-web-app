import { useState, useEffect, Fragment, useCallback, useRef } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Modal,
  Table,
  Accordion,
  Form,
  Tab,
  Tabs,
} from "react-bootstrap";
import Swal from "sweetalert2/dist/sweetalert2.all.min.js";
import { Howl } from "howler";
import DrumRoll from "./audioclips/Drumroll.mp3";
import Tada from "./audioclips/Tada.mp3";
import ReactCanvasConfetti from "react-canvas-confetti";
import * as xlsx from "xlsx";
import "./App.css";
import { Scrollbars } from "react-custom-scrollbars-2";

const canvasStyles = {
  position: "fixed",
  pointerEvents: "none",
  width: "100%",
  height: "100%",
  top: 0,
  left: 0,
};

function App() {
  const [randomizerData, setRandomizerData] = useState([]);
  const [randomizerUsed, setRandomizerUsed] = useState([]);
  const [randomizerDataOriginal, setRandomizerDataOriginal] = useState([]);
  const [randomizerDisplay, setRandomizerDisplay] = useState([]);
  const [excludedNumber, setExcludedNumber] = useState([]);
  const [showSecondDataText, setShowSecondDataText] = useState(false);
  const [showSecondDataButton, setShowSecondDataButton] = useState(false);
  const [disableDraw, setDisableDraw] = useState(false);
  const [modalShow, setModalShow] = useState(false);
  const [backgroundImage, setBackgroundImage] = useState("");
  const refAnimationInstance = useRef(null);
  const [arrayKeyNames, setArrayKeyNames] = useState([]);
  const [firstData, setFirstData] = useState();
  const [secondData, setSecondData] = useState();

  const getInstance = useCallback((instance) => {
    refAnimationInstance.current = instance;
  }, []);

  const makeShot = useCallback((particleRatio, opts) => {
    refAnimationInstance.current &&
      refAnimationInstance.current({
        ...opts,
        origin: { y: 0.7 },
        particleCount: Math.floor(200 * particleRatio),
      });
  }, []);

  const fire = useCallback(() => {
    makeShot(0.25, {
      spread: 26,
      startVelocity: 55,
    });

    makeShot(0.2, {
      spread: 60,
    });

    makeShot(0.35, {
      spread: 100,
      decay: 0.91,
      scalar: 0.8,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 25,
      decay: 0.92,
      scalar: 1.2,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 45,
    });
  }, [makeShot]);

  async function dataRandomizer(randomInt) {
    const timer = (ms) => new Promise((res) => setTimeout(res, ms));
    let loopCount = 30;
    setDisableDraw(true);
    SoundPlay(DrumRoll);
  
    for (let i = 0; i < loopCount; i++) {
      if (i === loopCount - 1) {
        setRandomizerDisplay(randomizerData[randomInt]);
        setRandomizerUsed([...randomizerUsed, randomizerData[randomInt]]);
        break;
      } else {
        const randomDataIndex = Math.floor(Math.random() * randomizerData.length);
        setRandomizerDisplay(randomizerData[randomDataIndex]);
        await timer(110);
      }
    }
  
    await timer(500);
    setShowSecondDataButton(true);
  }
  
  const drawRandomizerData = async () => {
    let randomInt = Math.floor(Math.random() * randomizerData.length);
    setShowSecondDataText(false);

    if (firstData?.length === undefined || secondData?.length === undefined) {
      Swal.fire({
        icon: "error",
        title: "Select Data",
        text: "Please select data in the options before starting.",
      });

      return;
    }

    if (randomizerData?.length === 0) {
      Swal.fire({
        icon: "error",
        title: "Randomizer Data List Empty",
        text: "Please upload excel data in the options before starting.",
      });

      return;
    } else {
      do {
        if (excludedNumber.includes(randomInt)) {
          randomInt = Math.floor(Math.random() * randomizerData.length);
        }

        if (excludedNumber.length === randomizerData.length) {
          Swal.fire({
            icon: "error",
            title: "Reset",
            text: "All randomizer data has appeared. Please reset the list.",
          });

          break;
        }

        if (!excludedNumber.includes(randomInt)) {
          setExcludedNumber([...excludedNumber, randomInt]);
          dataRandomizer(randomInt);
        }
      } while (excludedNumber.includes(randomInt));
    }
  };

  const winnerList = async () => {
    setModalShow(true);
  };

  const employeeReset = async () => {
    Swal.fire({
      title: "Are you sure you want to reset?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "RESET",
    }).then((result) => {
      if (result.isConfirmed) {
        setRandomizerData(randomizerDataOriginal);
        setExcludedNumber([]);
        setRandomizerDisplay([]);
        setRandomizerUsed([]);
        setDisableDraw(false);
        setShowSecondDataButton(false);
        setShowSecondDataText(false);
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Reset",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };

  const showData = async () => {
    setDisableDraw(false);
    setShowSecondDataButton(false);
    fire();
    SoundPlay(Tada);
    setShowSecondDataText(true);
  };

  const readUploadFile = async (e) => {
    e.preventDefault();

    if (e.target.files) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const data = e.target.result;
        const workbook = xlsx.read(data, { type: "array" });
        const sheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[sheetName];
        const json = xlsx.utils.sheet_to_json(worksheet);

        let tempData = json;
        let dataKeyNames = Object.keys(tempData[0]);

        setArrayKeyNames(dataKeyNames);

        tempData.map((data, i) => {
          return (tempData[i].isUsed = false);
        });

        setRandomizerData(tempData);
        setRandomizerDataOriginal(tempData);

        Swal.fire({
          position: "center",
          icon: "success",
          title: "Uploaded Data",
          showConfirmButton: false,
          timer: 1500,
        });

        document.getElementById("uploadData").value = "";
      };
      reader.readAsArrayBuffer(e.target.files[0]);
    }
  };

  const SoundPlay = (src) => {
    const sound = new Howl({
      src,
      html5: true,
    });
    sound.play();
  };

  const checkboxClick = (data, type) => {
    switch (type) {
      case 1:
        setFirstData(data);
        break;
      case 2:
        setSecondData(data);
        break;
      default:
    }
  };

  useEffect(() => {
    const fileInput = document.getElementById("fileInput");
    fileInput?.addEventListener("change", (e) => {
      const file = fileInput?.files[0];
      const reader = new FileReader();
      reader?.addEventListener("load", () => {
        setBackgroundImage(reader?.result);
      });
      if (file) {
        reader?.readAsDataURL(file);
      }
      document.getElementById("fileInput").value = "";
    });
  }, []);

  return (
    <Fragment>
      <Scrollbars>
        <Container
          fluid
          className="lottery-container h-100 m-0 p-0"
          style={{ backgroundImage: `url(${backgroundImage})` }}
        >
          <Row className="h-25 m-0 p-0">
            <Col className="col-12"></Col>
          </Row>
          <Row className="h-50 m-0 p-0">
            <Col className="d-flex align-items-center justify-content-center col-12 h-25">
              {randomizerDisplay ? (
                <Fragment>
                  <h3 className="text-randomizer">
                    <span className="text-randomizer-id">
                      {randomizerDisplay[firstData]}
                    </span>
                  </h3>
                </Fragment>
              ) : (
                <></>
              )}
            </Col>
            <Col className="h-50 name-container d-flex align-items-center justify-content-center col-12">
              {showSecondDataText === true ? (
                <Fragment>
                  <span className="text-randomizer-name">
                    {" "}
                    {randomizerDisplay[secondData]}{" "}
                  </span>
                </Fragment>
              ) : (
                <span></span>
              )}
            </Col>
            <Col className="h-25 col-12"></Col>
          </Row>

          <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles} />

          <Row className="h-25 m-0 p-0">
            <Col className="m-0 p-0 d-flex align-items-start justify-content-center col-12">
              <Row className="w-100 m-0 p-0">
                <Col className="col-4"></Col>
                <Col className="col-4 d-flex justify-content-center">
                  {disableDraw === false ? (
                    <Fragment>
                      <Button
                        size="lg"
                        variant="light"
                        className="draw-button"
                        onClick={() => drawRandomizerData()}
                        active
                      >
                        DRAW
                      </Button>
                    </Fragment>
                  ) : (
                    <Fragment>
                      {disableDraw === true && showSecondDataButton === true ? (
                        <Button
                          size="lg"
                          variant="info"
                          className="draw-button"
                          onClick={() => showData()}
                        >
                          SHOW
                        </Button>
                      ) : (
                        <></>
                      )}
                    </Fragment>
                  )}
                </Col>
                <Col className="col-4"></Col>
              </Row>
            </Col>
          </Row>

          <Accordion>
            <Accordion.Item eventKey="0">
              <Accordion.Header>OPTIONS (CLICK ME)</Accordion.Header>
              <Accordion.Body>
                <Tabs defaultActiveKey="file" className="mb-3">
                  <Tab eventKey="file" title="Upload File">
                    <Row>
                      <Col className="text-end">
                        <form>
                          <label
                            htmlFor="uploadData"
                            style={{
                              cursor: "pointer",
                              background: "grey",
                              padding: "5px",
                              color: "white",
                            }}
                          >
                            Upload Form Data
                          </label>
                          <input
                            className="d-none"
                            style={{ visibility: "hidden" }}
                            type="file"
                            name="uploadData"
                            id="uploadData"
                            onChange={readUploadFile}
                          />
                        </form>
                      </Col>
                      <Col className="text-start">
                        <form>
                          <label
                            htmlFor="fileInput"
                            style={{
                              cursor: "pointer",
                              background: "grey",
                              padding: "5px",
                              color: "white",
                            }}
                          >
                            Set Background Image
                          </label>
                          <input
                            className="d-none"
                            style={{ visibility: "hidden" }}
                            type="file"
                            name="fileInput"
                            id="fileInput"
                          />
                        </form>
                      </Col>
                    </Row>
                  </Tab>
                  <Tab eventKey="data" title="Select Data (IMPORTANT)">
                    <Row className="m-0 p-0">
                      <Col xs={12} className="radio-section">
                        {arrayKeyNames?.length !== 0 ? (
                          <Form className="d-flex justify-content-center ">
                            <div className="m-3 text-center">
                              <h3>1st Data: </h3>
                              {arrayKeyNames?.map((data) => {
                                return (
                                  <Form.Check
                                    inline
                                    label={data}
                                    name="group1"
                                    type="radio"
                                    id={`inline-${data}`}
                                    key={data}
                                    onClick={() => checkboxClick(data, 1)}
                                  />
                                );
                              })}
                            </div>
                          </Form>
                        ) : (
                          <h3 className="text-center">Empty Data</h3>
                        )}
                      </Col>
                      <Col xs={12} className="radio-section">
                        {arrayKeyNames?.length !== 0 ? (
                          <Form className="d-flex justify-content-center">
                            <div className="m-3 text-center">
                              <h3>2nd Data: </h3>
                              {arrayKeyNames?.map((data) => {
                                return (
                                  <Form.Check
                                    inline
                                    label={data}
                                    name="group2"
                                    type="radio"
                                    id={`inline-${data}`}
                                    key={data}
                                    onClick={() => checkboxClick(data, 2)}
                                  />
                                );
                              })}
                            </div>
                          </Form>
                        ) : (
                          <></>
                        )}
                      </Col>
                    </Row>
                  </Tab>
                  <Tab eventKey="drawlist" title="Draw List/Reset">
                    <Row>
                      <Col className="text-end">
                        <Button
                          size="lg"
                          variant="light"
                          className="winner-list-button border-radius-button"
                          onClick={() => winnerList()}
                          active
                        >
                          DRAW LIST
                        </Button>
                      </Col>
                      <Col className="text-start">
                        <Button
                          size="lg"
                          variant="light"
                          className="reset-button border-radius-button"
                          onClick={() => employeeReset()}
                          active
                        >
                          RESET
                        </Button>
                      </Col>
                    </Row>
                  </Tab>
                </Tabs>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </Container>
      </Scrollbars>

      <Modal
        show={modalShow}
        onHide={() => setModalShow(false)}
        size="lg"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            <h1 className="text-center">
              Raffle Draw List - Total Draw: {randomizerUsed.length}
            </h1>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>{firstData}</th>
                <th>{secondData}</th>
              </tr>
            </thead>
            <tbody>
              {randomizerUsed.length > 0 ? (
                randomizerUsed?.map((data, i) => (
                  <tr key={i}>
                    <td>
                      {i + 1} - {data[firstData]}
                    </td>
                    <td>{data[secondData]}</td>
                  </tr>
                ))
              ) : (
                <></>
              )}
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setModalShow(false)}>Close</Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
}

export default App;
